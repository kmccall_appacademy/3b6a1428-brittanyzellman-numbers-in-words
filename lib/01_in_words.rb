require 'byebug'

class Fixnum

  # alternative solution to all below, would need be replaced by in_words
  def in_words
    return "zero" if self == 0

    places = ["hundred", "thousand", "million", "billion",
              "trillion"]
    #create an array of the numbers to be separated in reverse order
    arr = self.to_s.chars.reverse
    i = 0
    j = 0
    # hash stores the 3 digit number corresponding to its place
    num_to_place = {}
    while i < arr.length
      num_to_place[places[j]] = [arr[i], arr[i += 1], arr[i += 1]]
      i += 1
      j += 1
    end
    hsh = join_and_reverse(num_to_place)
    return_num(hsh.keys.reverse, hsh.values.reverse)
  end

  def join_and_reverse(hsh)
    hsh.each do |k, v|
      v.compact if v.include?(nil)
      hsh[k] = v.reverse.join.to_i
    end
  end

  def return_num(arr_places, arr_values)
    return_str = ""
    i = 0
    while i < arr_values.length
      return_str += arr_values[i].evaluate(arr_places[i])
      i += 1
    end
    clean(return_str)
  end

  # returns a string
  # called on a three digit num and returns value as a string
  # depending on place
  def evaluate(place)
    str = ""
    ones = self % 10
    tens = (self % 100) - ones
    hundreds = self / 100
    arr = [hundreds, tens, ones]
    # debugger
    return str if arr.all? { |num| num == 0 }
    str += (hundreds.singles + " hundred ") if arr[0] > 0
    if arr[0] == 0 && arr[1] == 0 && place == "hundred"
      return arr[2].singles
    elsif arr[0] == 0 && arr[1] == 0
      return arr[2].singles + place
    end

    if (1..99).cover?(arr[1])
      str += ones.singles if arr[1] == 0
      str += (tens + ones).ten_to_twelve if (10..12).cover?(tens + ones)
      str += (tens + ones).teen if (13..19).cover?(tens + ones)
      str += (tens + ones).twenty_99 if (20..99).cover?(tens + ones)

    end

    return (str += " #{place} ") if place != "hundred"

    str
  end

  def clean(string)
    # debugger
    str = string.split(" ")
    str.delete(str.last) if str[-1] == " "
    str.join(" ")
  end


  #   if self % 100 == 0
  #     mod_zero
  #   else
  #     one = self % 10
  #     ten = self % 100
  #     hundred = self.mod_zero
  #     if (10..12).cover?(ten)
  #       return hundred + " " + ten.ten_to_twelve
  #     elsif (13..19).cover?(ten)
  #       return hundred + " " + ten.teen
  #     else
  #       return hundred + " " + (ten - one).tens + " " + one.singles
  #     end
  #   end
  # end

  # def in_words_1
  #   if (0..9).cover?(self)
  #     singles
  #   elsif (10..12).cover?(self)
  #     ten_to_twelve
  #   elsif (13..19).cover?(self)
  #     teen
  #   elsif (20..99).cover?(self)
  #     twenty_99
  #   elsif (100..999).cover?(self)
  #     hundred_999
  #   elsif (1000..999999).cover?(self)
  #     thousand_999999
  #   elsif (1000000..999999999).cover?(self)
  #     # debugger
  #     mill_trill
  #   end
  # end

  def singles
    case self
    when 0
      "zero"
    when 1
      "  one "
    when 2
      " two "
    when 3
      " three "
    when 4
      " four "
    when 5
      " five "
    when 6
      " six "
    when 7
      " seven "
    when 8
      " eight "
    when 9
      " nine "
    else
      raise "not an Integer"
    end
  end

  def ten_to_twelve
    case self
    when 10
      " ten "
    when 11
      " eleven "
    when 12
      " twelve "
    end
  end

  def teen
    case self
    when 13
      " thirteen "
    when 15
      " fifteen "
    when 18
      " eighteen "
    else
      ((self % 10).singles + "teen").delete(" ")
    end
  end

  def twenty_99
    if self % 10 == 0
      tens
    else
      (self - (self % 10)).tens + " " + (self % 10).singles
    end
  end

  # def hundred_999
  #   if self % 100 == 0
  #     mod_zero
  #   else
  #     one = self % 10
  #     ten = self % 100
  #     hundred = self.mod_zero
  #     if (10..12).cover?(ten)
  #       return hundred + " " + ten.ten_to_twelve
  #     elsif (13..19).cover?(ten)
  #       return hundred + " " + ten.teen
  #     else
  #       return hundred + " " + (ten - one).tens + " " + one.singles
  #     end
  #   end
  # end
  #
  # def thousand_999999
  #   if self % 1000 == 0
  #     mod_zero
  #   else
  #     hundred = (self % 1000).hundred_999
  #     thousand = self / 1000
  #     if thousand == 0
  #       return hundred
  #     elsif (10..12).cover?(thousand)
  #       return thousand.ten_to_twelve + " " + hundred
  #     elsif (13..19).cover?(thousand)
  #       return teen + " " + hundred
  #     else
  #       return thousand.twenty_99 + " thousand " + hundred
  #     end
  #   end
  # end
  #
  # def mill_trill
  #   if self % 100000 == 0
  #     mod_zero
  #   else
  #     debugger
  #     thousand = (self % 1000000).thousand_999999
  #     million = self / 1000000
  #     if million == 0
  #       return ""
  #     elsif (10..12).cover?(million)
  #       return million.ten_to_twelve + " million " + thousand
  #     elsif (13..19).cover?(million)
  #       return teen + " " + thousand
  #     else
  #       return million.twenty_99 + " million " + thousand_999999
  #     end
  #   end
  # end

  # def trill_bill
  #   if self % 100000000 == o
  #     mod_zero
  #   end
  # end

  # private

  def tens
    case self
    when 0
      ""
    when 20
      "twenty"
    when 30
      "thirty"
    when 40
      "forty"
    when 50
      "fifty"
    when 80
      "eighty"
    else
      ((self / 10).singles + "ty").delete(" ")
    end
  end

  # def mod_zero
  #   if (100..999).cover?(self)
  #     (self / 100).singles + " hundred"
  #   elsif (1000..999999).cover?(self)
  #     (self / 1000).singles + " thousand"
  #   elsif (1000000..999999999).cover?(self)
  #     (self / 1000000.singles + " million")
  #   elsif (1000000000..999999999999).cover?(self)
  #     (self / 1000000000).singles + " trillion"
  #   end
  # end

end
